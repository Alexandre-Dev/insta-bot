from instagrapi import Client
import os
import urllib.request
import ssl
import time

# Account details
username = 'username'
password = 'password'

# Content params
topic = 'ferrari'
desc = "«Les voitures de course ne sont ni belles ni laides. Elles sont belles quand elles gagnent» - Enzo Ferrari \n"
tags = '#ferrari #italy #car #cars #italia #porsche #lamborghini #supercar #f1 #mercedes #luxury #bmw #speed #supercars #formula1 #maranello #carsofinstagram #bugatti #racing #monza #alfaromeo #scuderiaferrari #bentley #astonmartin #maserati #love #milano #audi #millemiglia #race'

# Download the image with SSl
ssl._create_default_https_context = ssl._create_unverified_context

# Create the client
client = Client()

# Login
client.login(username, password)


while True:
    #Get the image
    urllib.request.urlretrieve(f'https://source.unsplash.com/featured?{topic}', 'image.jpg')

    # Upload the image
    client.photo_upload("image.jpg", desc + tags)

    if os.path.exists('image.jpg'):
        os.remove('image.jpg')

    # Wait 10 minutes
    time.sleep(600)
