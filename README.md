# Instagram Auto-Poster

## Overview
The Instagram Auto-Poster is a Python script designed to automate the process of posting images to an Instagram account. The script focuses on a specific topic, in this case, "Ferrari", and posts images related to this topic at regular intervals. This tool is especially useful for social media influencers, marketers, or anyone looking to maintain an active presence on Instagram with minimal manual effort.

## Features
- **Automated Posting**: The script automatically downloads and posts images to Instagram at set intervals.
- **Customizable Content**: Users can customize the topic, description, and hashtags for the posts.
- **SSL Support**: The script handles SSL verification for secure image downloading.
- **Continuous Operation**: Designed to run indefinitely until manually stopped, with a default wait time of 10 minutes between posts.

## Technologies Used
- **Python**: The script is written in Python, making it easy to run on any platform that supports Python.
- **Instagrapi**: A powerful Python library that allows the script to interact with the Instagram API.
- **Unsplash API**: The script uses the Unsplash API to source high-quality images related to the specified topic.
- **SSL Module**: For secure image downloading.

## How to Use
To use the Instagram Auto-Poster, follow these steps:

1. **Set Up Your Environment**: Ensure you have Python installed on your system.
2. **Install Dependencies**: Install the `instagrapi` library using pip:
`pip install instagrapi`
3. **Configure the Script**: Open the script and replace the `username` and `password` variables with your Instagram account credentials. Customize the `topic`, `desc`, and `tags` variables as desired.
4. **Run the Script**: Execute the script in your Python environment. The script will start posting images to your Instagram account at the specified intervals.

## Storage
The script temporarily stores the downloaded image as `image.jpg` in the current working directory. After each post, the image file is deleted to free up space.

## Running the Application
To run the Instagram Auto-Poster application, follow these steps:

1. Open a terminal or command prompt.
2. Navigate to the directory where the script is located.
3. Run the script by typing:
`python main.py`
4. The script will now start its operation, and you should see new posts appearing on your Instagram account every 10 minutes.

**Note**: It is important to comply with Instagram's terms of service when using this script. Automated posting can sometimes lead to account restrictions if abused or used too frequently.
